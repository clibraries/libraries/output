#ifndef __OUTPUT_HPP__
#define __OUTPUT_HPP__

#include <iostream>

// write the object to the standard output
template<typename T> void printto(std::ostream& os, const T& value){
    os << value << std::endl;
}

// write objects to the standard output
template<typename T, typename... Args> void printto(std::ostream& os, const T& value, const Args&... args){
    os << value << ' ';
    printto(os, args...); // recursive call
}

// write the object to the standard output
template<typename T> void print(const T& value){
    printto(std::cout, value);
}

// write objects to the standard output
template<typename T, typename... Args> void print(const T& value, const Args&... args){
    printto(std::cout, value, args...);
}

// write the object to the standard error output
template<typename T> void printerr(const T& value){
    printto(std::cerr, value);
}

// write objects to the standard error output
template<typename T, typename... Args> void printerr(const T& value, const Args&... args){
    printto(std::cerr, value, args...);
}

#endif